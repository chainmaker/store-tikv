/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package tikvdbprovider

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"testing"

	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/protocol/v2/test"
	"github.com/stretchr/testify/assert"
)

// UpdateBatch encloses the details of multiple `updates`
type UpdateBatch struct {
	kvs map[string][]byte
}

func (batch *UpdateBatch) Remove(key []byte) {
	delete(batch.kvs, string(key))
}

func (batch *UpdateBatch) Get(key []byte) ([]byte, error) {
	//TODO implement me
	panic("implement me")
}

func (batch *UpdateBatch) Has(key []byte) bool {
	//TODO implement me
	panic("implement me")
}

func (batch *UpdateBatch) SplitBatch(batchCnt uint64) []protocol.StoreBatcher {
	panic("implement me")
}

// NewUpdateBatch constructs an instance of a Batch
func NewUpdateBatch() protocol.StoreBatcher {
	return &UpdateBatch{kvs: make(map[string][]byte)}
}

// KVs return map
func (batch *UpdateBatch) KVs() map[string][]byte {
	return batch.kvs
}

// Put adds a KV
func (batch *UpdateBatch) Put(key []byte, value []byte) {
	//if value == nil {
	//	panic("Nil value not allowed")
	//}
	batch.kvs[string(key)] = value
}

// Delete deletes a Key and associated value
func (batch *UpdateBatch) Delete(key []byte) {
	batch.kvs[string(key)] = nil
}

// Len returns the number of entries in the batch
func (batch *UpdateBatch) Len() int {
	return len(batch.kvs)
}

// Merge merges other kvs to this updateBatch
func (batch *UpdateBatch) Merge(u protocol.StoreBatcher) {
	for key, value := range u.KVs() {
		batch.kvs[key] = value
	}
}

// var dbPath = "./"
var log = &test.GoLogger{}

func initTikvHosts() error {
	cmd := exec.Command("/bin/bash", "./start_tikv.sh")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("start tikv error :", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		if !strings.Contains(errStr, "No such container") {
			return fmt.Errorf(errStr)
		}
	}
	if strings.Contains(outStr, "error") {
		return fmt.Errorf(errStr)
	}

	return nil
}

func destroyTikvHosts() {
	cmd := exec.Command("/bin/bash", "./stop_tikv.sh")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		fmt.Println("stop tikv error: ", err)
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	outStr, errStr := stdout.String(), stderr.String()
	fmt.Printf("out:%s\n", outStr)
	if errStr != "" {
		fmt.Printf("err:%s\n", errStr)
	}
}

var conf = &TiKVDbConfig{
	Endpoints: "127.0.0.1:2379",
}

var op = &NewTikvDBOptions{
	Config:    conf,
	Logger:    log,
	Encryptor: nil,
	ChainId:   "chain1",
}

func TestNewTikvDBHandle(t *testing.T) {
	defer func() {
		err := recover()
		fmt.Println(err)
	}()

	assert.Nil(t, initTikvHosts())
	defer destroyTikvHosts()

	dbHandle := NewTiKVDBHandle(op)

	key1 := []byte("key1")
	value1 := []byte("value1")
	err := dbHandle.Put(key1, value1)
	assert.Nil(t, err)
	dbHandle.Close()

	//Files cannot be created in folders that do not have permissions
	dbHandle = NewTiKVDBHandle(op)
	assert.NotNil(t, dbHandle)
}

func TestDBHandle_Put(t *testing.T) {
	assert.Nil(t, initTikvHosts())
	defer destroyTikvHosts()

	dbHandle := NewTiKVDBHandle(op)
	//defer dbHandle.Close()

	key1 := []byte("key1")
	value1 := []byte("value1")
	err := dbHandle.Put(key1, value1)
	assert.Nil(t, err)

	has, err := dbHandle.Has(key1)
	assert.True(t, has)
	assert.Nil(t, err)

	err = dbHandle.Put(key1, nil)
	assert.Equal(t, strings.Contains(err.Error(), "error writing tikvdbprovider with nil value"), true)

	value, err := dbHandle.Get(key1)
	assert.Nil(t, err)
	assert.Equal(t, value1, value)
	value, err = dbHandle.Get([]byte("another key"))
	assert.Nil(t, err)
	assert.Nil(t, value)

	err = dbHandle.Delete(key1)
	assert.Nil(t, err)

	has, err = dbHandle.Has(key1)
	assert.False(t, has)
	assert.Nil(t, err)

	dbHandle.Close()
	value, err = dbHandle.Get(key1)
	assert.Equal(t, strings.Contains(err.Error(), "error"), true)
	assert.Nil(t, value)

	has, err = dbHandle.Has([]byte("key"))
	assert.False(t, has)
	assert.Equal(t, strings.Contains(err.Error(), "error"), true)

	//err = dbHandle.Put(key1, value1)
	//assert.Equal(t, strings.Contains(err.Error(), "DB Closed"), true)
}

func TestDBHandle_WriteBatch(t *testing.T) {
	assert.Nil(t, initTikvHosts())
	defer destroyTikvHosts()

	dbHandle := NewTiKVDBHandle(op) //dbPath：db文件的存储路径
	defer dbHandle.Close()
	batch := NewUpdateBatch()
	key1 := []byte("key1")
	value1 := []byte("value1")
	key2 := []byte("key2")
	value2 := []byte("value2")
	batch.Put(key1, value1)
	batch.Put(key2, value2)
	err := dbHandle.WriteBatch(batch, true)
	assert.Nil(t, err)

	value, err := dbHandle.Get(key2)
	assert.Nil(t, err)
	assert.Equal(t, value2, value)

	key3 := []byte("key3")
	batch.Put(key3, nil)
	err = dbHandle.WriteBatch(batch, true)
	assert.Nil(t, err)

	value3, err := dbHandle.Get(key3)
	assert.Nil(t, err)
	assert.Nil(t, value3)
}

func TestDBHandle_Gets(t *testing.T) {
	assert.Nil(t, initTikvHosts())
	defer destroyTikvHosts()

	dbHandle := NewTiKVDBHandle(op) //dbPath：db文件的存储路径
	defer dbHandle.Close()

	n := 3
	keys := make([][]byte, 0, n+1)
	values := make([][]byte, 0, n+1)
	batch := NewUpdateBatch()
	for i := 0; i < n; i++ {
		keyi := []byte(fmt.Sprintf("key%d", i))
		valuei := []byte(fmt.Sprintf("value%d", i))
		keys = append(keys, keyi)
		values = append(values, valuei)

		batch.Put(keyi, valuei)
	}

	err := dbHandle.WriteBatch(batch, true)
	assert.Nil(t, err)

	keys = append(keys, nil)
	values = append(values, nil)
	valuesR, err1 := dbHandle.GetKeys(keys)
	assert.Nil(t, err1)
	for i := 0; i < len(keys); i++ {
		assert.Equal(t, values[i], valuesR[i])
	}
}

func TestDBHandle_NewIteratorWithRange(t *testing.T) {
	assert.Nil(t, initTikvHosts())
	defer destroyTikvHosts()

	op.Config.MaxScanLimit = 10
	op.Config.ScanBatchSize = 10
	dbHandle := NewTiKVDBHandle(op) //dbPath：db文件的存储路径
	defer dbHandle.Close()

	batch := NewUpdateBatch()
	key1 := []byte("key1")
	value1 := []byte("value1")
	key2 := []byte("key2")
	value2 := []byte("value2")
	batch.Put(key1, value1)
	batch.Put(key2, value2)
	err := dbHandle.WriteBatch(batch, true)
	assert.Nil(t, err)

	var iter protocol.Iterator
	iter, err = dbHandle.NewIteratorWithRange([]byte("key1"), []byte("key3"))
	assert.Nil(t, err)
	defer iter.Release()
	var count int
	for iter.Next() {
		count++
		showIterData(iter)
	}
	assert.Equal(t, 2, count)
	if iter.First() {
		assert.Equal(t, key1, iter.Key())
		assert.Equal(t, value1, iter.Value())
		showIterData(iter)
	}

	iter, err = dbHandle.NewIteratorWithRange([]byte(""), []byte(""))
	assert.Nil(t, iter)
	assert.Equal(t, strings.Contains(err.Error(), "iterator range should not start() or limit() with empty key"), true)
}

func TestDBHandle_NewIteratorWithPrefix(t *testing.T) {
	assert.Nil(t, initTikvHosts())
	defer destroyTikvHosts()

	op.Config.MaxScanLimit = 10
	op.Config.ScanBatchSize = 2
	dbHandle := NewTiKVDBHandle(op) //dbPath：db文件的存储路径
	defer dbHandle.Close()

	batch := NewUpdateBatch()

	key1 := []byte("key1")
	value1 := []byte("value1")
	batch.Put(key1, value1)
	batch.Put([]byte("key2"), []byte("value2"))
	batch.Put([]byte("key3"), []byte("value3"))
	batch.Put([]byte("key4"), []byte("value4"))
	batch.Put([]byte("keyx"), []byte("value5"))

	err := dbHandle.WriteBatch(batch, true)
	assert.Equal(t, nil, err)

	var iter protocol.Iterator
	iter, err = dbHandle.NewIteratorWithPrefix([]byte("key"))
	assert.Nil(t, err)
	defer iter.Release()
	var count int
	for iter.Next() {
		count++
		showIterData(iter)
	}
	assert.Equal(t, 5, count)
	if iter.First() {
		assert.Equal(t, key1, iter.Key())
		assert.Equal(t, value1, iter.Value())
		showIterData(iter)
	}

	iter, err = dbHandle.NewIteratorWithPrefix([]byte(""))
	assert.Nil(t, iter)
	assert.Equal(t, strings.Contains(err.Error(), "iterator range should not start() or limit() with empty key"), true)
}

func TestTempFolder(t *testing.T) {
	t.Log(os.TempDir())
}

func showIterData(iter protocol.Iterator) {
	fmt.Printf("key: %s, value: %s\n", string(iter.Key()), string(iter.Value()))
}
