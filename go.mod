module chainmaker.org/chainmaker/store-tikv/v2

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.4
	chainmaker.org/chainmaker/protocol/v2 v2.3.5
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pkg/errors v0.9.1
	github.com/prometheus/procfs v0.6.0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/tikv/client-go v1.0.0
)

replace (
	github.com/tikv/client-go => chainmaker.org/third_party/tikv-client-go v1.0.0
	google.golang.org/grpc v1.40.0 => google.golang.org/grpc v1.26.0
)
