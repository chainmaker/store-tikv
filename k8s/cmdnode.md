// deploy
kubectl create -f chainmaker-pd-pv.yaml
kubectl create -f chainmaker-tikv-pv.yaml

kubectl create -f crd.yaml
kubectl create namespace tidb-admin
kubectl create -f tidb-operator.yaml
kubectl get pods --namespace tidb-admin -l app.kubernetes.io/instance=tidb-operator

kubectl create namespace tidb-cluster
kubectl create -f tidb-cluster.yaml
kubectl get po -n tidb-cluster
kubectl create -f tidb-monitor.yaml


// delete
kubectl delete namespace tidb-admin
kubectl delete namespace tidb-cluster
kubectl delete -f chainmaker-pd-pv.yaml
kubectl delete -f chainmaker-tikv-pv.yaml
kubectl delete -f crd.yaml
kubectl delete -f tidb-operator.yaml

// port-word
kubectl port-forward --address 0.0.0.0 -n tidb-cluster svc/basic-pd 2379:2379
